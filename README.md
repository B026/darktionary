# Darktionary

It's a so simple dictionary webapp which holds concepts related to computer forensics. It's built using pure HTML5, CSS, JS and PHP.

## Contributors
+ Alberto Castro Estrada
+ Mildred Samantha Hernández Torres
+ Jorge Hernández Clemente
